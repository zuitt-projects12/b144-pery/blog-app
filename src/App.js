import Posts from "./components/Posts/Posts";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="text-center my-3 text-white">My Blog</h1>
      </header>
      <Posts/>
    </div>
  );
}

export default App;
