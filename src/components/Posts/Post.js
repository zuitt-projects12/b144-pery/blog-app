import "./Post.css";
const Post = (
    { 
        post: { title, body , imgUrl, author},
        index 
    }) => {
    return (
        <div className="post-container">
            <h1 className="heading">{title}</h1>
            <img src={imgUrl} alt="post" className="image" />
            <p>{body}</p>
        </div>
    )
}

export default Post
